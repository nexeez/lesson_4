#!/usr/bin/env python3
__author__ = 'Kacper Nyk'
__email__ = 'szkolatechi@op.pl'
""" If/else """
import sys


# ------------------------------------------

# number = input("Give me a number: ")
# number = int(number)
#
# if number > 0:
#    print("Your number is greater than 0.")
# elif number < 0:
#    print("Your number is lower than 0.")
# else:
#    print("Your number is 0.")

# ------------------------------------------

# name = input("Give me a name: ")
#
# if not name:
#    print("Your name is empty!")
#    sys.exit(1)
#
# pi = 5
# if name in "Kacper":
#    pi = 3.14
# elif name == "Adam":
#    pi = 100
#
# print(pi)

# ------------------------------------------

# number = input("Give me a number: ")
# if number.isalpha():
#     print("Your value is not a number")
#     sys.exit(2)
#
# number = int(number)
#
# if 0 <= number <= 5:
#     print("Number between 0 and 5.")

# ------------------------------------------

# def compare(other: str):
#     return (other == "Kacper") and (other in "Kacper Nyk")
#
#
# print(compare(input("Give me a name: ")))

# ------------------------------------------

# class Horse:
#     def __init__(self, name: str):
#         self.name = name
#
#     def __eq__(self, other):
#         return self.name == other.name
#
#
# horacy = Horse("Zawisza")
# zawisza = Horse("Zawisza")
#
# print(horacy == zawisza, horacy, zawisza)

# ------------------------------------------

# class Human:
#     def __init__(self, name: str, is_alive: bool):
#         self.name = name
#         self.is_alive = is_alive
#
#     def __bool__(self):
#         return self.is_alive
#
#
# a, b, c = Human("a", True), Human("b", True), Human("c", False)
# print(bool(a))
# print(bool(b))
# print(bool(c))

# ------------------------------------------


