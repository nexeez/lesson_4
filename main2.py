#!/usr/bin/env python3
__author__ = 'Kacper Nyk'
__email__ = 'szkolatechi@op.pl'
""" for/while """

#---------------------------------------

# for pos, char in range("Kacper"):
#     print(pos, char)

#---------------------------------------

# for num in range(10, 2, -1):
#     print(num)

#---------------------------------------

# number = input()
# number = int(number)
#
# while number > 10:
#     print(number)
#     number -= 1

#---------------------------------------

# number = input()
# number = int(number)
#
# while True:
#     print(number)
#     number -= 1
#     if number == 20:
#         number -= 5
#         continue
#     if number < 10:
#         break

#---------------------------------------

# number = input()
# number = int(number)
#
# for char in "ABCDEFG":
#     if char in "XYZ":
#         continue
#     print(char)

#---------------------------------------

# number = input()
# number = int(number)
#
# var = "ODADOZ"
# for pos, char in enumerate(var):
#     print(char, pos, var.index(char))

#---------------------------------------

# from random import randrange
#
# num = 0
#
# while num != 15:
#     num = randrange(5, 25)
#     print(num)

#---------------------------------------


