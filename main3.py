#!/usr/bin/env python3
__author__ = 'Kacper Nyk'
__email__ = 'szkolatechi@op.pl'
""" container """


# ------------------------------------------------

# objects = list() jesli nie znamy wartosci
# objects = [1, 2, 3, 4]  # jesli znamy wartosci
#
# for obj in objects:
#     print(obj)

# ------------------------------------------------

# objects = [
#     [1, 2, 3, 4],
#     [1, 2, 3, 4],
#     [1, 2, 3, 4]
# ]
#
# for l in objects:
#     for el in l:
#         print(el, end="")
#     print()

# ------------------------------------------------

# objects = [
#     [1, 2, 3, 4],
#     [1, 2, 3, 4],
#     [1, 2, 3, 4]
# ]
#
# print(objects[0][2]) # [0] - pierwszy wiersz [2] - 3 pozycja w wierszu

# ------------------------------------------------

# objects = list()
# for i in range(10, 50):
#     objects.append(i)
#
# print(objects[30:35])
# print(objects[30:])
# print(objects[30:-1])
# print(objects[10::2])
# print(objects[:10])
# print(objects[:10:2])

# ------------------------------------------------

### Comprehensions
# objects = [i for i in range(10, 50) if i & 2 == 0]
#
# print(objects)
# print(objects[30:35])
# print(objects[30:])
# print(objects[30:-1])
# print(objects[10::2])
# print(objects[:10])
# print(objects[:10:2])

# ------------------------------------------------

# def func(first, *args, **kwargs):
#     print(args)
#     print(first)
#
#
# objects = [1, 2, 3, 4, 5]
# func(*objects)

# ------------------------------------------------

# objects = [10, 44, 420, 12, 5, 6, 8]
# objects2 = ["b", "A", "Elo", "elo"]
# objects.sort()
# objects2.sort()
# print(objects)
# print(objects2)

# ------------------------------------------------

# a = [1, 2, 3, None, None]
# print(a)
# print(a.count(None))
# print(a)
#
# print(a)
# a.remove(None)
# print(a)
#
# print(bool(a))
# print(a.pop())
# print(a.pop())
# print(a.pop())
# print(a)
# print(bool(a))
#
# if a:
#     print("ELO")
# else:
#     print("NIE MA MNIE")

# ------------------------------------------------

### Krotka, brak edycji na listach, stala lista
# a = (3, 4, 1, 2, 5)
# print(a.count((3)))
# print(a.index((3)))
# print(a)

# ------------------------------------------------

# a = tuple(['a', 'b', 'c'])
# b = list((1, 2, 3, 4, 5))
# print(type(a), type(b))

# ------------------------------------------------

a = list(i for i in range(40))
print(a)
x, y, z = a[:10], a[10:20], a[20:]
print(x, y, z)

